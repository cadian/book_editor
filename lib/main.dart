import 'package:book_editor/ui/book_editor.dart';
import 'package:book_editor/util/theme_notifier.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:responsive_framework/responsive_framework.dart';

const String MOBILE_SMALL_DEVICE = 'MOBILE_SMALL_DEVICE';
const String MOBILE_DEVICE = 'MOBILE_DEVICE';
const String TABLET_DEVICE = 'TABLET_DEVICE';
const String DESKTOP_DEVICE = 'DESKTOP_DEVICE';
const String DESKTOP_LARGE_DEVICE = 'DESKTOP_LARGE_DEVICE';

void main() {
  Logger(printer: PrettyPrinter(methodCount: 0));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ThemeMode>(
        valueListenable: ThemeNotifier().themeMode,
        builder: (ctx, value, child) {
          return MaterialApp(
            title: 'Flutter Demo',
            theme: FlexThemeData.light(scheme: FlexScheme.bigStone),
            darkTheme: FlexThemeData.dark(scheme: FlexScheme.redWine),
            debugShowCheckedModeBanner: false,
            themeMode: ThemeNotifier().themeMode.value,
            builder: (ctx, child) => ResponsiveWrapper.builder(BouncingScrollWrapper.builder(context, child!),
                defaultScaleFactor: 1.0,
                defaultScaleFactorLandscape: 1.0,
                breakpoints: [
                  ResponsiveBreakpoint.resize(300, name: MOBILE_SMALL_DEVICE, scaleFactor: 0.8),
                  ResponsiveBreakpoint.resize(450, name: MOBILE_DEVICE, scaleFactor: 0.8),
                  ResponsiveBreakpoint.resize(700, name: MOBILE_DEVICE, scaleFactor: 0.8),
                  ResponsiveBreakpoint.resize(1000, name: TABLET_DEVICE, scaleFactor: 0.9),
                  ResponsiveBreakpoint.resize(2460, name: DESKTOP_DEVICE, scaleFactor: 0.9),
                ],
                breakpointsLandscape: [
                  ResponsiveBreakpoint.resize(300, name: MOBILE_SMALL_DEVICE, scaleFactor: 0.6),
                  ResponsiveBreakpoint.resize(450, name: MOBILE_DEVICE, scaleFactor: 0.7),
                  ResponsiveBreakpoint.resize(700, name: MOBILE_DEVICE, scaleFactor: 0.7),
                  ResponsiveBreakpoint.resize(1000, name: TABLET_DEVICE, scaleFactor: 0.8),
                  ResponsiveBreakpoint.resize(2460, name: DESKTOP_DEVICE, scaleFactor: 0.9),
                ],
                background: Container(color: Color(0xFFF5F5F5))),
            home: BookEditor(),
          );
        });
  }
}
