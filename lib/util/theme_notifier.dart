import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeNotifier {
  ThemeNotifier._privateConstructor();

  ValueNotifier<ThemeMode> themeMode = ValueNotifier<ThemeMode>(ThemeMode.dark);

  static final ThemeNotifier _instance = ThemeNotifier._privateConstructor();

  factory ThemeNotifier() {
    return _instance;
  }

  void toggleTheme() {
    final brightness = this.themeMode.value;
    if (brightness == ThemeMode.dark) {
      this.themeMode.value = ThemeMode.light;
    } else {
      this.themeMode.value = ThemeMode.dark;
    }
  }
}
