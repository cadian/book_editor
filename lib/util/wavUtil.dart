import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:book_editor/util/dub_log.dart';
import 'package:book_editor/util/exceptions.dart';
import 'package:ffmpeg_kit_flutter/ffmpeg_kit_config.dart';
import 'package:ffmpeg_kit_flutter/ffprobe_kit.dart';
import 'package:ffmpeg_kit_flutter/media_information.dart';
import 'package:ffmpeg_kit_flutter/return_code.dart';

enum WaveformType { summary, accurate }

extension MediaInformationX on MediaInformation {
  bool isAudioFile() {
    final String format = getFormat() ?? "unknown";
    return format == "wav" || format == "mp3" || format == "flac";
  }

  String? getSampleRate() {
    try {
      return getStreams().first.getSampleRate();
    } on Exception catch (_) {
      return null;
    }
  }
}

class FFmpegService {
  static const int _defaultSampleRate = 44100;
  static const String astatsMetaData = "lavfi.astats.Overall.RMS_peak";
  // static const String astatsMetaData = "lavfi.astats.Overall.Peak_level";

  static Future<List<double>?> getAudioFilePeakLevel(
    AudioFilePeakLevelInput input,
    double start,
    double end,
  ) async {
    final audioFile = input.audioFile;
    final waveformType = input.waveformType;

    await FFmpegKitConfig.disableLogs();
    // await FFmpegKitConfig.enableLogs();
    await FFmpegKitConfig.setLogLevel(16);

    final mediaInformationSession = await FFprobeKit.getMediaInformation(audioFile.path);
    final mediaInformation = mediaInformationSession.getMediaInformation();

    if (mediaInformation != null && mediaInformation.isAudioFile()) {
      final command = _getFFprobeCommand(
          audioFile: audioFile, mediaInformation: mediaInformation, waveformType: waveformType, start: start, end: end);

      DubLog().d('command ${command}');

      final _completer = Completer<List<double>?>();

      await FFprobeKit.executeAsync(command, (session) async {
        final returnCode = await session.getReturnCode();
        final output = await session.getOutput();

        if (ReturnCode.isSuccess(returnCode)) {
          _completer.complete(_outputToList(output));
        } else {
          _handleFailure(returnCode: returnCode, completer: _completer);
        }
      });

      return _completer.future;
    } else {
      throw NotAnAudioFileException();
    }
  }

  static List<double> _outputToList(String? output) {
    if (output == null || output.isEmpty) {
      throw EmptyOutputException();
    }

    try {
      final parsedOutput = jsonDecode(output);
      final frameData = parsedOutput["frames"] as List;
      return frameData
          .map<double?>((e) {
            try {
              return 1 / (double.parse(e["tags"][astatsMetaData] as String));
            } catch (e) {
              return null;
            }
          })
          .whereType<double>()
          .toList();
    } on Exception catch (_) {
      throw ParsingOutputException();
    }
  }

  static void _handleFailure({
    required ReturnCode? returnCode,
    required Completer<List<double>?> completer,
  }) {
    if (ReturnCode.isCancel(returnCode)) {
      DubLog().i("ffprobe commande canceled");
    } else {
      final e = FFprobeCommandeFailedException();
      completer.completeError(e);
    }
  }

  static String _getFFprobeCommand({
    required File audioFile,
    required MediaInformation mediaInformation,
    required WaveformType waveformType,
    required double start,
    required double end,
  }) {
    final audioFilePath = audioFile.path;
    final sampleRate = waveformType == WaveformType.summary ? _getSampleRate(mediaInformation: mediaInformation) : "";

    double duration = end - start;

    return "-v error -f lavfi -i \"amovie=$audioFilePath$sampleRate,"
        "atrim=start=$start:end=$end,"
        "asetnsamples=${duration * 20},"
        "astats=metadata=1:reset=1\" -show_entries"
        " frame_tags=$astatsMetaData -of json";
  }

  static String _getSampleRate({required MediaInformation mediaInformation}) {
    const sampleRateKey = "asetnsamples";
    final sampleRateValue = mediaInformation.getSampleRate() ?? _defaultSampleRate.toString();
    return ",$sampleRateKey=$sampleRateValue";
  }
}

class AudioFilePeakLevelInput {
  const AudioFilePeakLevelInput({
    required this.audioFile,
    required this.waveformType,
  });

  factory AudioFilePeakLevelInput.fromEncodedData(
    Map<String, String> encodedData,
  ) =>
      AudioFilePeakLevelInput(
        audioFile: File(encodedData["audioFile"]!),
        waveformType: WaveformType.values[int.parse(encodedData["waveformType"]!)],
      );

  Map<String, String> endode() => {"audioFile": audioFile.path, "waveformType": waveformType.index.toString()};

  final File audioFile;
  final WaveformType waveformType;
}
