import 'package:logger/logger.dart';

class DubLog {
  late Logger logger;

  static final DubLog _instance = DubLog._internal();

  factory DubLog() {
    return _instance;
  }

  d(String message) {
    logger.d(message);
  }

  i(String message) {
    logger.i(message);
  }

  e(String message) {
    logger.e(message);
  }

  changeLoggingEnable() {
    logger = Logger(filter: LogEnableFilter());
  }

  DubLog._internal() {
    logger = Logger(printer: PrettyPrinter(methodCount: 0));
  }
}

class LogEnableFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    return true;
  }
}
