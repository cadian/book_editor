import 'package:book_editor/model/safe_convert.dart';

class ChapterModel {
  int seqNo;
  String audioURL;
  Title title;
  List<ParagraphsItem> paragraphs;

  ChapterModel({
    this.seqNo = 0,
    this.audioURL = "",
    required this.title,
    required this.paragraphs,
  });

  factory ChapterModel.fromJson(Map<String, dynamic>? json) => ChapterModel(
        seqNo: asInt(json, 'seqNo'),
        audioURL: asString(json, 'audioURL'),
        title: Title.fromJson(asMap(json, 'title')),
        paragraphs: asList(json, 'paragraphs').map((e) => ParagraphsItem.fromJson(e)).toList(),
      );

  Map<String, dynamic> toJson() {
    return {
      'seqNo': this.seqNo,
      'audioURL': this.audioURL,
      'title': this.title.toJson(),
      'paragraphs': this.paragraphs.map((e) => e.toJson()).toList(),
    };
  }
}

class Title {
  int seqNo;
  String text;
  bool isLast;
  bool isTitle;
  Audio audio;
  List<WordsItem> words;

  Title({
    this.seqNo = 0,
    this.text = "",
    this.isLast = false,
    this.isTitle = false,
    required this.audio,
    required this.words,
  });

  factory Title.fromJson(Map<String, dynamic>? json) => Title(
        seqNo: asInt(json, 'seqNo'),
        text: asString(json, 'text'),
        isLast: asBool(json, 'isLast'),
        isTitle: asBool(json, 'isTitle'),
        audio: Audio.fromJson(asMap(json, 'audio')),
        words: asList(json, 'words').map((e) => WordsItem.fromJson(e)).toList(),
      );

  Map<String, dynamic> toJson() {
    return {
      'seqNo': this.seqNo,
      'text': this.text,
      'isLast': this.isLast,
      'isTitle': this.isTitle,
      'audio': this.audio.toJson(),
      'words': this.words.map((e) => e.toJson()).toList(),
    };
  }
}

class Audio {
  double start;
  double end;
  double duration;

  Audio({
    this.start = 0.0,
    this.end = 0.0,
    this.duration = 0.0,
  });

  factory Audio.fromJson(Map<String, dynamic>? json) => Audio(
        start: asDouble(json, 'start'),
        end: asDouble(json, 'end'),
        duration: asDouble(json, 'duration'),
      );

  Map<String, dynamic> toJson() => {
        'start': this.start,
        'end': this.end,
        'duration': this.duration,
      };
}

class ParagraphsItem {
  int seqNo;
  Audio audio;
  List<SentencesItem> sentences;

  ParagraphsItem({
    this.seqNo = 0,
    required this.audio,
    required this.sentences,
  });

  factory ParagraphsItem.fromJson(Map<String, dynamic>? json) => ParagraphsItem(
        seqNo: asInt(json, 'seqNo'),
        audio: Audio.fromJson(asMap(json, 'audio')),
        sentences: asList(json, 'sentences').map((e) => SentencesItem.fromJson(e)).toList(),
      );

  Map<String, dynamic> toJson() => {
        'seqNo': this.seqNo,
        'audio': this.audio.toJson(),
        'sentences': this.sentences.map((e) => e.toJson()).toList(),
      };
}

class SentencesItem {
  int seqNo;
  String text;
  bool isLast;
  bool isTitle;
  Audio audio;
  List<WordsItem> words;

  SentencesItem({
    this.seqNo = 0,
    this.text = "",
    this.isLast = false,
    this.isTitle = false,
    required this.audio,
    required this.words,
  });

  factory SentencesItem.fromJson(Map<String, dynamic>? json) => SentencesItem(
        seqNo: asInt(json, 'seqNo'),
        text: asString(json, 'text'),
        isLast: asBool(json, 'isLast'),
        isTitle: asBool(json, 'isTitle'),
        audio: Audio.fromJson(asMap(json, 'audio')),
        words: asList(json, 'words').map((e) => WordsItem.fromJson(e)).toList(),
      );

  Map<String, dynamic> toJson() => {
        'seqNo': this.seqNo,
        'text': this.text,
        'isLast': this.isLast,
        'isTitle': this.isTitle,
        'audio': this.audio.toJson(),
        'words': this.words.map((e) => e.toJson()).toList(),
      };
}

class WordsItem {
  String text;
  String cleanText;
  Audio audio;

  WordsItem({
    this.text = "",
    this.cleanText = "",
    required this.audio,
  });

  factory WordsItem.fromJson(Map<String, dynamic>? json) => WordsItem(
        text: asString(json, 'text'),
        cleanText: asString(json, 'clean_text'),
        audio: Audio.fromJson(asMap(json, 'audio')),
      );

  Map<String, dynamic> toJson() => {
        'text': this.text,
        'clean_text': this.cleanText,
        'audio': this.audio.toJson(),
      };
}
