import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonFlat extends StatelessWidget {
  final Widget child;
  final VoidCallback onPressed;
  const ButtonFlat({Key? key, required this.child, required this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
        cursor: SystemMouseCursors.click,
        child: CupertinoButton(padding: EdgeInsets.all(8),
            pressedOpacity: .8,
            onPressed: onPressed, child: child));
  }
}
