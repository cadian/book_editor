import 'package:book_editor/component/ButtonFlat.dart';
import 'package:book_editor/main.dart';
import 'package:book_editor/model/chaptorModel.dart';
import 'package:book_editor/ui/provider/book_editor_provider.dart';
import 'package:book_editor/ui/widgets/pageDrawer.dart';
import 'package:book_editor/ui/widgets/polygonWave.dart';
import 'package:book_editor/ui/widgets/wordCard.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_wrapper.dart';

class SaveIntent extends Intent {
  const SaveIntent();
}

class BookEditor extends StatefulWidget {
  const BookEditor({Key? key}) : super(key: key);

  @override
  State<BookEditor> createState() => _BookEditorState();
}

class _BookEditorState extends State<BookEditor> {
  late BookEditorProvider provider;
  ScrollController mainController = ScrollController();
  ScrollController editController = ScrollController();

  @override
  void initState() {
    provider = BookEditorProvider(this);
    super.initState();
  }

  @override
  void dispose() {
    provider.dispose();
    mainController.dispose();
    editController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<BookEditorProvider>.value(
        value: provider,
        child: Shortcuts(
          shortcuts: <LogicalKeySet, Intent>{
            LogicalKeySet(LogicalKeyboardKey.meta, LogicalKeyboardKey.keyS): SaveIntent(),
            LogicalKeySet(LogicalKeyboardKey.meta, LogicalKeyboardKey(0x00000003134)): SaveIntent(),
          },
          child: Actions(
            actions: <Type, Action<Intent>>{
              SaveIntent: CallbackAction<SaveIntent>(onInvoke: (SaveIntent intent) {
                provider.saveJson();
                return;
              }),
            },
            child: Consumer<BookEditorProvider>(
                builder: (ctx, prov, child) {
                  if (!prov.focusNode.hasFocus) {
                    prov.focusNode.requestFocus();
                  }
                  return Focus(autofocus: true, focusNode: prov.focusNode, child: child!);
                },
                child: Scaffold(
                    key: provider.scaffoldKey,
                    appBar: AppBar(
                      title: Consumer<BookEditorProvider>(builder: (ctx, prov, child) {
                        String title = '';
                        if (prov.bookModel != null) {
                          title = 'EDIT : ${provider.selectedPageSeqNo}';
                        }
                        return Text('$title');
                      }),
                      actions: [
                        Consumer<BookEditorProvider>(builder: (ctx, prov, child) {
                          if (prov.bookModel != null) {
                            return ButtonFlat(
                                onPressed: () {
                                  provider.reset();
                                },
                                child: Icon(
                                  CupertinoIcons.refresh,
                                  color: Theme.of(context).colorScheme.secondary,
                                ));
                          }
                          return SizedBox();
                        }),
                        Consumer<BookEditorProvider>(builder: (ctx, prov, child) {
                          if (prov.bookModel != null) {
                            return ButtonFlat(
                                onPressed: () {
                                  provider.resetProvider();
                                },
                                child: Icon(
                                  CupertinoIcons.trash,
                                  color: Theme.of(context).colorScheme.secondary,
                                ));
                          }
                          return SizedBox();
                        }),
                      ],
                      centerTitle: true,
                      leading: Builder(builder: (context) {
                        return ButtonFlat(
                            onPressed: () {
                              Scaffold.of(context).openDrawer();
                            },
                            child: Icon(
                              CupertinoIcons.bars,
                              color: Theme.of(context).colorScheme.secondary,
                            ));
                      }),
                    ),
                    drawer: PageDrawer(),
                    body: Stack(
                      children: [
                        Consumer<BookEditorProvider>(builder: (context, prov, child) {
                          return Builder(builder: (ctx) {
                            if (prov.bookModel == null) {
                              return buildAddJson();
                            } else if (prov.audioFile == null) {
                              return buildAddWav();
                            } else {
                              return buildMainView();
                            }
                          });
                        }),
                        buildProgress()
                      ],
                    ))),
          ),
        ));
  }

  Widget buildMainView() {
    if (ResponsiveWrapper.of(context).activeBreakpoint.name == MOBILE_DEVICE) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(flex: 4, child: buildScriptView()),
          Divider(height: 2, color: Theme.of(context).colorScheme.outline),
          Expanded(flex: 3, child: buildEditView()),
        ],
      );
    } else {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
              flex: 7,
              child: buildScriptView(
                constraint: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
              )),
          VerticalDivider(width: 2, color: Theme.of(context).colorScheme.outline),
          Expanded(
              flex: 2,
              child: buildEditView(
                constraint: BoxConstraints(minHeight: MediaQuery.of(context).size.height),
              )),
        ],
      );
    }
  }

  Widget buildScriptView({constraint}) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Column(
          children: [
            Expanded(
              flex: 5,
              child: SingleChildScrollView(
                controller: mainController,
                child: Container(
                  constraints: constraint,
                  padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Builder(builder: (context) {
                        return Wrap(
                          spacing: -1,
                          runSpacing: 4,
                          children: [
                            for (SentencesItem sentence in provider.currentPage!.sentences) ...{
                              for (WordsItem word in sentence.words) ...{
                                if (sentence.seqNo != provider.selectedSentenceSeqNo) ...{
                                  WordCard(
                                    word: word,
                                    selectedSeq: provider.selectedSentenceSeqNo,
                                    onPressed: (seqNo) {
                                      provider.playSentence(seqNo: seqNo);
                                    },
                                    currentPosition: Duration.zero,
                                    seqNo: sentence.seqNo,
                                    isTitle: sentence.isTitle,
                                    isLastWord: word == sentence.words.last,
                                  ),
                                } else ...{
                                  StreamBuilder<Duration>(
                                      stream: provider.playPositionStream.distinct(),
                                      initialData: Duration.zero,
                                      builder: (context, snapshot) {
                                        Duration? curPos = snapshot.data;
                                        return WordCard(
                                          word: word,
                                          selectedSeq: provider.selectedSentenceSeqNo,
                                          onPressed: (seqNo) {
                                            provider.playSentence(seqNo: seqNo);
                                          },
                                          currentPosition: curPos ?? Duration.zero,
                                          seqNo: sentence.seqNo,
                                          isTitle: sentence.isTitle,
                                          isLastWord: word == sentence.words.last,
                                        );
                                      })
                                },
                              },
                              if (sentence.isLast || sentence.isTitle) ...{
                                SizedBox(height: 12, width: double.infinity),
                              }
                            },
                          ],
                        );
                      }),
                    ],
                  ),
                ),
              ),
            ),
            Divider(height: 1, color: Theme.of(context).colorScheme.outline),
            Expanded(
              flex: 3,
              child: LayoutBuilder(builder: (context, layout) {
                SentencesItem? sentence = provider.expandedSentences
                    .firstOrNullWhere((element) => element.seqNo == provider.selectedSentenceSeqNo);

                if (sentence == null) {
                  return SizedBox();
                }

                double startPos = sentence.audio.start;
                double endPos = sentence.audio.end;
                return StreamBuilder<Duration>(
                    stream: provider.playPositionStream.distinct(),
                    initialData: Duration.zero,
                    builder: (context, snapshot) {
                      Duration? curPos = snapshot.data;

                      return WaveForms(
                        provider: provider,
                        endPos: endPos,
                        startPos: startPos,
                        progress: curPos!,
                      );
                    });
              }),
            )
          ],
        ),
        Positioned(
          right: 16,
          bottom: 16,
          child: FloatingActionButton(
            child: Icon(CupertinoIcons.down_arrow),
            onPressed: () async {
              await provider.saveJson();
            },
          ),
        ),
      ],
    );
  }

  Widget buildEditView({constraint}) {
    SentencesItem? item = provider.sentenceFromSeqNo(seqNo: provider.selectedSentenceSeqNo);
    SentencesItem? beforeItem = provider.sentenceFromSeqNo(seqNo: provider.selectedSentenceSeqNo - 1);

    return SingleChildScrollView(
      controller: editController,
      child: Container(
        key: ValueKey(provider.selectedSentenceSeqNo),
        constraints: constraint,
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.secondary.withOpacity(.3),
        ),
        padding: EdgeInsets.only(top: 16, right: 16, left: 16, bottom: 36),
        child: item == null
            ? Center(child: Text('대사를 선택해주세요'))
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'sentence options',
                    style: TextStyle(fontSize: 18, color: Theme.of(context).colorScheme.secondary),
                  ),
                  SizedBox(height: 16),
                  buildOptionSwitchTile(
                      title: 'isLast',
                      value: item.isLast,
                      onChanged: () {
                        item.isLast = !item.isLast;
                      }),
                  buildOptionSwitchTile(
                      title: 'isTitle',
                      value: item.isTitle,
                      onChanged: () {
                        item.isTitle = !item.isTitle;
                      }),
                  Divider(height: 36),
                  Padding(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'audio options',
                          style: TextStyle(fontSize: 18, color: Theme.of(context).colorScheme.secondary),
                        ),
                        buildOptionNumberTile(
                            title: 'audio start',
                            value: item.audio.start,
                            onChanged: (value) {
                              provider.setStartPointInSentence(sentence: item, applyValue: value);
                            }),
                        buildOptionNumberTile(
                            title: 'audio end',
                            value: item.audio.end,
                            onChanged: (value) {
                              provider.setEndPointInSentence(sentence: item, applyValue: value);
                            }),
                        buildOptionNumberTile(
                            title: 'audio duration',
                            value: item.audio.duration,
                            onChanged: (value) {
                              provider.setDurationPointInSentence(sentence: item, applyValue: value);
                            }),
                      ],
                    ),
                  ),
                  Divider(height: 36),
                  Text(
                    'words',
                    style: TextStyle(fontSize: 18, color: Theme.of(context).colorScheme.secondary),
                  ),
                  SizedBox(height: 16),
                  ...item.words.map((word) {
                    return ExpansionTile(
                      childrenPadding: EdgeInsets.only(bottom: 16, top: 16, left: 16),
                      title: Text('${word.text}'),
                      expandedAlignment: Alignment.centerLeft,
                      tilePadding: EdgeInsets.zero,
                      expandedCrossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (item.words.first == word && (beforeItem?.isLast ?? false)) ...{
                          Text(
                            'merge',
                            style: TextStyle(fontSize: 18, color: Theme.of(context).colorScheme.secondary),
                          ),
                          buildOptionButtonTile(
                              title: 'merge',
                              onPressed: () {
                                provider.mergeByWord(sentence: item, previousSentence: beforeItem!, word: word);
                              }),
                          Divider(height: 16),
                        } else if (item.words.last != word) ...{
                          Text(
                            'split',
                            style: TextStyle(fontSize: 18, color: Theme.of(context).colorScheme.secondary),
                          ),
                          buildOptionButtonTile(
                              title: 'split',
                              onPressed: () {
                                provider.splitByWord(sentence: item, word: word);
                              }),
                          Divider(height: 16),
                        },
                        Text(
                          'word options',
                          style: TextStyle(fontSize: 18, color: Theme.of(context).colorScheme.secondary),
                        ),
                        buildOptionTextTile(
                            title: 'word',
                            value: word.text,
                            onChanged: (value) {
                              provider.setTextInWord(sentence: item, word: word, applyValue: value);
                            }),
                        Divider(height: 16),
                        Text(
                          'audio options',
                          style: TextStyle(fontSize: 18, color: Theme.of(context).colorScheme.secondary),
                        ),
                        buildOptionNumberTile(
                            title: 'audio start',
                            value: word.audio.start,
                            onChanged: (value) {
                              provider.setStartPointInWord(word: word, applyValue: value);
                            }),
                        buildOptionNumberTile(
                            title: 'audio end',
                            value: word.audio.end,
                            onChanged: (value) {
                              provider.setEndPointInWord(word: word, applyValue: value);
                            }),
                        buildOptionNumberTile(
                            title: 'audio duration',
                            value: word.audio.duration,
                            onChanged: (value) {
                              provider.setDurationPointInWord(word: word, applyValue: value);
                            }),
                        SizedBox(height: 16),
                        MouseRegion(
                          cursor: SystemMouseCursors.click,
                          child: CupertinoButton(
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              provider.playSentenceFromWord(seqNo: item.seqNo, startPoint: word.audio.start);
                            },
                            child: Center(
                                child: Container(
                              padding: EdgeInsets.symmetric(vertical: 8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(width: 1, color: Theme.of(context).colorScheme.outline),
                                  color: Theme.of(context).colorScheme.background),
                              child: Center(
                                child: Text(
                                  '듣기',
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ),
                            )),
                          ),
                        ),
                      ],
                    );
                  }).toList(),
                ],
              ),
      ),
    );
  }

  Widget buildOptionSwitchTile({required String title, required bool value, required VoidCallback onChanged}) {
    return Row(
      children: [
        Text(
          '$title',
          style: TextStyle(fontSize: 16),
        ),
        Spacer(),
        Transform.scale(
            scale: .8,
            child: CupertinoSwitch(
                value: value,
                onChanged: (changedValue) {
                  onChanged.call();
                  provider.changeBoolean(option: title, before: value, after: changedValue);
                })),
      ],
    );
  }

  Widget buildOptionTextTile({required String title, required String value, required ValueChanged<String> onChanged}) {
    return Row(
      children: [
        Text(
          '$title',
          style: TextStyle(fontSize: 16),
        ),
        Spacer(),
        ButtonFlat(
            child: Text(
              '$value',
              style: TextStyle(decoration: TextDecoration.underline),
            ),
            onPressed: () async {
              String? text = await provider.showTextChangeDialog(value);
              onChanged.call(text ?? value);
            })
      ],
    );
  }

  Widget buildOptionNumberTile(
      {required String title, required double value, required ValueChanged<double> onChanged}) {
    String fixedValue = value.toStringAsFixed(2);
    double fixedDoubleValue = double.parse(fixedValue);
    return Row(
      children: [
        Text(
          '$title',
          style: TextStyle(fontSize: 16),
        ),
        Spacer(),
        ButtonFlat(
            child: Text(
              '${fixedDoubleValue}',
              style: TextStyle(decoration: TextDecoration.underline),
            ),
            onPressed: () async {
              double? number = await provider.showNumberChangeDialog(fixedDoubleValue);
              onChanged.call(number ?? fixedDoubleValue);
            })
      ],
    );
  }

  Widget buildOptionButtonTile({required String title, required VoidCallback onPressed}) {
    return Row(
      children: [
        Text(
          '$title',
          style: TextStyle(fontSize: 16),
        ),
        Spacer(),
        ButtonFlat(
            child: Text(
              '${title}',
              style: TextStyle(decoration: TextDecoration.underline),
            ),
            onPressed: () async {
              onPressed.call();
            })
      ],
    );
  }

  Widget buildProgress() {
    return ValueListenableBuilder<bool>(
        valueListenable: provider.progress,
        builder: (ctx, value, child) {
          if (value) {
            return Center(
                child: CupertinoActivityIndicator(
              radius: 50,
            ));
          } else {
            return SizedBox();
          }
        });
  }

  Widget buildAddJson() {
    return Center(
      child: ButtonFlat(
        onPressed: provider.pickJsonFile,
        child: Column(
          children: [
            Spacer(),
            Icon(CupertinoIcons.add, size: 50),
            SizedBox(height: 24),
            Text('JSON 파일 선택하기'),
            Spacer(),
          ],
        ),
      ),
    );
  }

  Widget buildAddWav() {
    return Center(
      child: ButtonFlat(
        onPressed: provider.pickWavFile,
        child: Column(
          children: [
            Spacer(),
            Icon(CupertinoIcons.add, size: 50),
            SizedBox(height: 24),
            Text('WAV 파일 선택하기'),
            Spacer(),
          ],
        ),
      ),
    );
  }
}

class WaveForms extends StatefulWidget {
  const WaveForms({
    Key? key,
    required this.provider,
    required this.endPos,
    required this.startPos,
    required this.progress,
  }) : super(key: key);

  final BookEditorProvider provider;
  final double endPos;
  final double startPos;
  final Duration progress;

  @override
  State<WaveForms> createState() => _WaveFormsState();
}

class _WaveFormsState extends State<WaveForms> {
  List<double> samples = [];
  WordsItem? selectedWord;
  Offset? selectedOffset;

  @override
  void setState(VoidCallback fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  Future getSamples({required double end, required double start}) async {
    setState(() {
      samples = [];
    });
    samples = await widget.provider.generateWaveform(end: end, start: start);
    setState(() {
      samples;
    });
  }

  @override
  void didUpdateWidget(WaveForms oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.startPos != widget.startPos) {
      selectedOffset = null;
      selectedWord = null;
      getSamples(start: widget.startPos, end: widget.endPos);
    }
  }

  @override
  void initState() {
    getSamples(end: widget.endPos, start: widget.startPos);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, layout) {
      double width = (layout.maxWidth) - 36;

      SentencesItem? sentence = widget.provider.expandedSentences
          .firstOrNullWhere((element) => element.seqNo == widget.provider.selectedSentenceSeqNo);

      if (sentence == null) {
        return SizedBox();
      }

      double startPos = sentence.audio.start;
      double endPos = sentence.audio.end;
      double duration = (endPos - startPos);
      double widthPerSecond = (width / duration);

      return Builder(builder: (context) {
        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Builder(builder: (context) {
            List<double> waves = samples;

            if (waves.isEmpty) {
              return Center(child: CircularProgressIndicator());
            }

            int index = 0;

            return Padding(
              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              child: Container(
                width: width,
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    GestureDetector(
                      onHorizontalDragStart: (detail) {
                        setState(() {
                          selectedWord = null;
                          selectedOffset = detail.localPosition;
                        });
                      },
                      onHorizontalDragUpdate: (detail) {
                        if (detail.localPosition.dx > width || detail.localPosition.dx < 0) {
                          return;
                        }
                        setState(() {
                          selectedWord = null;
                          selectedOffset = detail.localPosition;
                        });
                      },
                      child: PolygonWave(
                        samples: waves,
                        height: layout.maxHeight,
                        width: width,
                      ),
                    ),
                    ...sentence.words.map((word) {
                      index++;
                      double start = word.audio.start;
                      double end = word.audio.end;

                      double wordStartPos = widthPerSecond * ((start - .1) - startPos);
                      double wordWidth = widthPerSecond * (end - start);
                      return Positioned(
                        left: wordStartPos,
                        top: index.isEven ? 30 : 0,
                        width: wordWidth,
                        height: 30,
                        child: MouseRegion(
                          cursor: SystemMouseCursors.click,
                          child: GestureDetector(
                            onTap: () async {
                              setState(() {
                                selectedWord = word;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                    width: .5,
                                    color: selectedWord == word
                                        ? Theme.of(context).colorScheme.primary
                                        : Theme.of(context).colorScheme.outline),
                                color: Theme.of(context).colorScheme.background,
                              ),
                              child: FittedBox(
                                child: IgnorePointer(
                                  child: WordCard(
                                    seqNo: sentence.seqNo,
                                    currentPosition: widget.progress,
                                    isLastWord: false,
                                    word: word,
                                    isTitle: false,
                                    selectedSeq: sentence.seqNo,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    }).toList(),
                    if (selectedOffset != null) ...{
                      Positioned(
                        top: -30,
                        left: selectedOffset!.dx,
                        child: Stack(
                          children: [
                            Container(
                              width: 1,
                              height: layout.maxHeight,
                              color: Colors.white,
                            ),
                            Container(
                              transform: Matrix4.translationValues(0, -30, 0),
                              child: Builder(builder: (context) {
                                return Text('${offsetToPosition(width, selectedOffset!)}');
                              }),
                            )
                          ],
                        ),
                      )
                    },
                    if (selectedWord != null) ...{
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          decoration: BoxDecoration(
                              color: Theme.of(context).colorScheme.background,
                              border: Border.all(width: 1, color: Theme.of(context).colorScheme.outline)),
                          child: Builder(builder: (context) {
                            return Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                OutlinedButton(
                                    child: Text('${selectedWord!.text}'),
                                    onPressed: () async {
                                      String? text = await widget.provider.showTextChangeDialog(selectedWord!.text);

                                      if (text != null) {
                                        widget.provider
                                            .setTextInWord(sentence: sentence, word: selectedWord!, applyValue: text);
                                      }
                                    }),
                                SizedBox(width: 8),
                                Text('start'),
                                SizedBox(width: 8),
                                OutlinedButton(
                                    child: Text('-'),
                                    onPressed: () {
                                      widget.provider.setStartPointInWord(
                                          word: selectedWord!, applyValue: selectedWord!.audio.start - 0.01);
                                    }),
                                OutlinedButton(
                                    child: Text('+'),
                                    onPressed: () {
                                      widget.provider.setStartPointInWord(
                                          word: selectedWord!, applyValue: selectedWord!.audio.start + 0.01);
                                    }),
                                SizedBox(width: 8),
                                Text('duration'),
                                SizedBox(width: 8),
                                OutlinedButton(
                                    child: Text('-'),
                                    onPressed: () {
                                      widget.provider.setDurationPointInWord(
                                          word: selectedWord!, applyValue: selectedWord!.audio.duration - 0.01);
                                    }),
                                OutlinedButton(
                                    child: Text('+'),
                                    onPressed: () {
                                      widget.provider.setDurationPointInWord(
                                          word: selectedWord!, applyValue: selectedWord!.audio.duration + 0.01);
                                    }),
                              ],
                            );
                          }),
                        ),
                      )
                    },
                    ...List.generate(11, (index) => (width / 11) * index).map((dx) {
                      return Positioned(
                          left: dx,
                          top: -40,
                          child: Stack(
                            children: [
                              Container(
                                width: 1,
                                height: layout.maxHeight,
                                color: Colors.white.withOpacity(.3),
                              ),
                              Text('${offsetToPosition(width, Offset(dx, 0)).toStringAsFixed(2)}')
                            ],
                          ));
                    }).toList()
                      ..add(Positioned(
                          left: width,
                          top: -40,
                          child: Stack(
                            children: [
                              Container(
                                width: 1,
                                height: layout.maxHeight,
                                color: Colors.white.withOpacity(.3),
                              ),
                              Text('${offsetToPosition(width, Offset(width, 0)).toStringAsFixed(2)}')
                            ],
                          )))
                  ],
                ),
              ),
            );
          }),
        );
      });
    });
  }

  double offsetToPosition(double width, Offset offset) {
    SentencesItem? sentence = widget.provider.expandedSentences
        .firstOrNullWhere((element) => element.seqNo == widget.provider.selectedSentenceSeqNo);

    double factor = (offset.dx / width);
    double selectedPos = (factor * (sentence!.audio.end - sentence.audio.start)) + sentence.audio.start;
    return selectedPos;
  }
}
