import 'dart:math';

import 'package:flutter/material.dart';

class PolygonWave extends StatelessWidget {
  final List<double> samples;
  final double height;
  final double width;

  const PolygonWave({Key? key, required this.samples, required this.height, required this.width}) : super(key: key);

  List<double> processSamples(var data) {
    final rawSamples = samples;

    var _processedSamples = rawSamples.map((e) => e * height).toList();

    final maxNum = _processedSamples.reduce((a, b) => max(a.abs(), b.abs()));
    final minNum = _processedSamples.reduce((a, b) => min(a.abs(), b.abs()));
    _processedSamples = _processedSamples.map((e) => e = e + minNum).toList();
    if (maxNum > 0) {
      final multiplier = pow(maxNum, -1).toDouble();
      final finalHeight = height / 2;
      final finalMultiplier = multiplier * finalHeight;

      _processedSamples = _processedSamples
          .map(
            (e) => e * finalMultiplier,
          )
          .toList();
    }

    return _processedSamples;
  }

  @override
  Widget build(BuildContext context) {
    final processedSamples = processSamples(samples);
    final sampleWidth = width / (processedSamples.length);

    return Stack(
      children: [
        RepaintBoundary(
          child: CustomPaint(
            size: Size(width, height),
            isComplex: true,
            painter: PolygonInActiveWaveformPainter(
                samples: processedSamples, sampleWidth: sampleWidth, color: Theme.of(context).colorScheme.primary),
          ),
        ),
      ],
    );
  }
}

class PolygonInActiveWaveformPainter extends CustomPainter {
  PolygonInActiveWaveformPainter({
    required this.samples,
    required this.sampleWidth,
    required this.color,
  });

  List<double> samples;
  double sampleWidth;
  Color color;

  /// Style of the waveform

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.stroke;

    final pathTop = Path();
    final pathBottom = Path();

    for (var i = 0; i < samples.length; i++) {
      final x = sampleWidth * i;
      final y = samples[i];

      pathTop.lineTo(x, 0);
      pathTop.lineTo(x, y);
      pathTop.lineTo(x, 0);
    }

    for (var i = 0; i < samples.length; i++) {
      final x = sampleWidth * i;
      final y = samples[i];

      pathBottom.lineTo(x, 0);
      pathBottom.lineTo(x, -y);
      pathBottom.lineTo(x, 0);
    }

    final shiftedPath = pathTop.shift(Offset(0, size.height / 2));
    final shiftedPath2 = pathBottom.shift(Offset(0, size.height / 2));

    canvas.drawPath(shiftedPath, paint);
    canvas.drawPath(shiftedPath2, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
