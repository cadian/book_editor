import 'package:book_editor/component/ButtonFlat.dart';
import 'package:book_editor/model/chaptorModel.dart';
import 'package:book_editor/ui/provider/book_editor_provider.dart';
import 'package:book_editor/util/theme_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PageDrawer extends StatefulWidget {
  const PageDrawer({Key? key}) : super(key: key);

  @override
  State<PageDrawer> createState() => _PageDrawerState();
}

class _PageDrawerState extends State<PageDrawer> {
  @override
  Widget build(BuildContext context) {
    return Consumer<BookEditorProvider>(
        builder: (ctx, prov, child) => Drawer(child: Material(child: Builder(
              builder: (ctx) {
                return Column(
                  children: [
                    SizedBox(
                      height: 56,
                      child: Row(
                        children: [
                          SizedBox(width: 16),
                          Text('${ThemeNotifier().themeMode.value}'),
                          Spacer(),
                          Transform.scale(
                              scale: .8,
                              child: CupertinoSwitch(
                                  value: ThemeNotifier().themeMode.value == ThemeMode.light,
                                  onChanged: (changedValue) {
                                    ThemeNotifier().toggleTheme();
                                  })),
                        ],
                      ),
                    ),
                    CupertinoSegmentedControl<String>(
                        groupValue: '${prov.tabType}',
                        children: {
                          'PAGE': Padding(padding: EdgeInsets.symmetric(horizontal: 8), child: Text('PAGE')),
                          'HISTORY': Padding(padding: EdgeInsets.symmetric(horizontal: 8), child: Text('HISTORY')),
                        },
                        onValueChanged: (value) {
                          prov.tabType = value;
                          prov.notifyListeners();
                        }),
                    Divider(height: 36),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            if (prov.tabType == 'PAGE') ...{
                              ...prov.bookModel?.paragraphs.map((paragraph) => buildPageTile(paragraph)).toList() ?? []
                            } else ...{
                              ...prov.historys.reversed.map((history) => buildHistoryTile(history)),
                            }
                          ],
                        ),
                      ),
                    )
                  ],
                );
              },
            ))));
  }

  Widget buildPageTile(ParagraphsItem paragraph) {
    var provider = Provider.of<BookEditorProvider>(context, listen: false);

    var isSelected = provider.selectedPageSeqNo == paragraph.seqNo;
    var color = isSelected ? Theme.of(context).colorScheme.primary : Theme.of(context).colorScheme.secondary;
    return ButtonFlat(
      onPressed: () {
        provider.setPageSeqNo(paragraph.seqNo);
      },
      child: Row(
        children: [
          Spacer(),
          if (isSelected) ...{
            Icon(CupertinoIcons.pin, color: color),
          },
          SizedBox(width: 8),
          Text(
            '${paragraph.seqNo}',
            style: TextStyle(fontSize: 18, color: color),
          ),
          Spacer(),
        ],
      ),
    );
  }

  Widget buildHistoryTile(HistoryModel history) {
    return ButtonFlat(
      onPressed: () {},
      child: Row(
        children: [
          Flexible(
            child: Text(
              '${history.option} : ${history.before} ➡ ${history.after}',
              style: TextStyle(fontSize: 18),
            ),
          ),
        ],
      ),
    );
  }
}
