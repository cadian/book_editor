import 'dart:math';

import 'package:book_editor/model/chaptorModel.dart';
import 'package:flutter/material.dart';

typedef WordPressHandler = void Function(int seqNo);

class WordCard extends StatelessWidget {
  final WordsItem word;
  final int selectedSeq;
  final Duration currentPosition;
  final WordPressHandler? onPressed;
  final bool isTitle;
  final bool isLastWord;
  final int seqNo;

  WordCard({
    Key? key,
    required this.word,
    required this.isTitle,
    required this.isLastWord,
    required this.selectedSeq,
    required this.seqNo,
    required this.currentPosition,
    this.onPressed,
  }) : super(key: key);

  bool checkIsActive() {
    Duration startPoint = Duration(milliseconds: (word.audio.start * 1000).toInt());
    if (currentPosition > startPoint) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isSelected = seqNo == selectedSeq;
    TextStyle backStyle;
    TextStyle frontStyle;
    double fontSize = 24;

    backStyle = TextStyle(decoration: isSelected ? TextDecoration.underline : TextDecoration.none, fontSize: fontSize);

    frontStyle = TextStyle(color: Colors.red, fontSize: fontSize);

    if (isTitle) {
      backStyle = backStyle.copyWith(fontWeight: FontWeight.w500, fontSize: fontSize);

      frontStyle = frontStyle.copyWith(fontWeight: FontWeight.w500, fontSize: fontSize);
    }

    Duration endPoint = Duration(milliseconds: (word.audio.end * 1000).toInt());
    Tween<double> tween = Tween<double>(begin: 0, end: 1);
    Duration duration = Duration(milliseconds: max(0, (word.audio.duration * 1000).toInt()));
    if ((currentPosition > endPoint)) {
      tween = Tween<double>(begin: 1, end: 1);
      duration = Duration.zero;
    }
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
          onTap: () {
            onPressed?.call(seqNo);
          },
          child: Stack(
            children: [
              Padding(
                  padding: EdgeInsets.only(right: isLastWord ? 4 : 0),
                  child: Text('${word.text}${isLastWord ? '' : ' '}', style: backStyle)),
              if (checkIsActive() && isSelected) ...{
                TweenAnimationBuilder<double>(
                    tween: tween,
                    duration: duration,
                    builder: (BuildContext context, double size, Widget? child) {
                      return ClipRRect(
                        child: Align(
                          alignment: AlignmentDirectional(-1.0, -1.0),
                          widthFactor: size,
                          child: child,
                        ),
                      );
                    },
                    child: Text('${word.text}${isLastWord ? '' : ' '}', style: frontStyle))
              }
            ],
          )),
    );
  }
}
