import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:book_editor/component/ButtonFlat.dart';
import 'package:book_editor/model/chaptorModel.dart';
import 'package:book_editor/model/safe_convert.dart';
import 'package:book_editor/util/dub_log.dart';
import 'package:book_editor/util/wavUtil.dart';
import 'package:dartx/dartx.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:file_saver/file_saver.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:just_audio/just_audio.dart';
import 'package:rxdart/subjects.dart';

class HistoryModel {
  String option;
  var before;
  var after;

  HistoryModel({required this.option, required this.before, required this.after});
}

class BookEditorProvider with ChangeNotifier {
  final GlobalKey scaffoldKey = GlobalKey();
  final FocusNode focusNode = FocusNode();
  final ValueNotifier<bool> progress = ValueNotifier(false);
  final AudioPlayer player = AudioPlayer(handleInterruptions: false, handleAudioSessionActivation: false);
  final BehaviorSubject<Duration> playPositionStream = BehaviorSubject.seeded(Duration.zero);
  final State state;

  FilePickerCross? originfile;
  ChapterModel? bookModel;
  Uint8List? audioFile;

  List<double>? waveForms;

  int selectedPageSeqNo = 1;
  int selectedSentenceSeqNo = -1;

  DateTime latestPlayTime = DateTime.now();

  List<HistoryModel> historys = [];

  String tabType = 'PAGE';

  BookEditorProvider(this.state);

  void resetProvider() {
    originfile = null;
    bookModel = null;
    audioFile = null;
    waveForms = null;
    selectedPageSeqNo = 1;
    selectedSentenceSeqNo = -1;
    historys = [];
    tabType = 'PAGE';
    notifyListeners();
  }

  @override
  void dispose() {
    debugPrint('disposed!');
    playPositionStream.close();
    focusNode.dispose();
    player.dispose();
    progress.dispose();
    super.dispose();
  }

  List<SentencesItem> get expandedSentences =>
      bookModel?.paragraphs.expand((element) => element.sentences).toList() ?? [];

  ParagraphsItem? get currentPage =>
      bookModel?.paragraphs.firstOrNullWhere((element) => element.seqNo == selectedPageSeqNo);

  SentencesItem? sentenceFromSeqNo({required seqNo}) =>
      expandedSentences.firstOrNullWhere((element) => element.seqNo == seqNo);

  Future setAudioPlayer() async {
    await player.setAudioSource(BufferAudioSource(audioFile!)).onError((error, stackTrace) {
      DubLog().e('setAudioPlayer error :$error : $stackTrace');
    });
  }

  void setProgress(bool value) {
    progress.value = value;
  }

  void setCurrentPos(Duration position) {
    if (playPositionStream.isClosed) {
      return;
    }
    playPositionStream.add(position);
  }

  void setPageSeqNo(int seqNo) {
    int totalPageCount = bookModel?.paragraphs.length ?? 0;
    if (seqNo <= 0) {
      return;
    }
    if (seqNo > totalPageCount) {
      return;
    }

    selectedPageSeqNo = seqNo;
    debugPrint('selectedPageSeqNo = ${selectedPageSeqNo}');

    notifyListeners();
  }

  void setCurrentSeq({required int seqNo, bool withNotifier = true}) {
    selectedSentenceSeqNo = seqNo;
    if (withNotifier) {
      notifyListeners();
    }
  }

  Future playSentenceFromWord({required int seqNo, required double startPoint}) async {
    SentencesItem? sentence = sentenceFromSeqNo(seqNo: seqNo);
    if (sentence != null) {
      DateTime time = latestPlayTime = DateTime.now();
      var startPos = Duration(milliseconds: ((startPoint * 1000) - 200).toInt());
      var endPoint = Duration(milliseconds: (sentence.audio.end * 1000).toInt());
      await Future.wait([
        stopMainPlayer(withResetSentence: false),
        player.seek(startPos),
      ]);
      setCurrentSeq(seqNo: seqNo);
      setCurrentPos(startPos + Duration(milliseconds: 200));
      player.play();
      await delayWhilePlayerStart(player, startPos);
      await generateProgress(time: time, startPosition: startPos, endPosition: endPoint);
      if (time == latestPlayTime) {
        stopMainPlayer(withResetSentence: false);
      }
    }
  }

  Future playSentence({required int seqNo}) async {
    SentencesItem? sentence = sentenceFromSeqNo(seqNo: seqNo);
    if (sentence != null) {
      DateTime time = latestPlayTime = DateTime.now();
      var startPoint = Duration(milliseconds: (sentence.audio.start * 1000).toInt());
      var endPoint = Duration(milliseconds: (sentence.audio.end * 1000).toInt());
      await Future.wait([
        stopMainPlayer(withResetSentence: false),
        player.seek(startPoint),
      ]);

      setCurrentSeq(seqNo: seqNo);
      player.play();
      await delayWhilePlayerStart(player, startPoint);
      await generateProgress(time: time, startPosition: startPoint, endPosition: endPoint);
      if (time == latestPlayTime) {
        stopMainPlayer(withResetSentence: false);
      }
    }
  }

  Future generateProgress(
      {required DateTime time, required Duration startPosition, required Duration endPosition}) async {
    try {
      final int adjustFactor = 200;
      int startTime = DateTime.now().millisecondsSinceEpoch + startPosition.inMilliseconds;
      int endTime = DateTime.now().millisecondsSinceEpoch + endPosition.inMilliseconds + adjustFactor;
      int initTime = DateTime.now().millisecondsSinceEpoch;
      int increasedTime = 0;

      for (; startTime <= endTime && state.mounted;) {
        if (time != latestPlayTime) {
          return;
        }
        if (!state.mounted) {
          return;
        }
        await Future.delayed(Duration(milliseconds: 50));

        startTime = DateTime.now().millisecondsSinceEpoch + startPosition.inMilliseconds;

        increasedTime = increasedTime + (DateTime.now().millisecondsSinceEpoch - initTime);
        setCurrentPos(Duration(milliseconds: startTime - initTime));
      }
    } catch (e) {
      DubLog().e('generate Progress error =${e}');
    }
  }

  Future pickJsonFile() async {
    try {
      FilePickerCross myFile = await FilePickerCross.importFromStorage(type: FileTypeCross.any, fileExtension: 'json');
      originfile = myFile;
      String json = myFile.toString();
      bookModel = ChapterModel.fromJson(jsonDecode(json));
      DubLog().d('Json file loaded!');
      notifyListeners();
    } catch (e) {
      if (e.runtimeType != FileSelectionCanceledError) {
        DubLog().e('error in pick json $e');
      }
    }
  }

  Future pickWavFile() async {
    try {
      setProgress(true);
      FilePickerCross myFile = await FilePickerCross.importFromStorage(type: FileTypeCross.audio, fileExtension: 'wav');
      audioFile = await myFile.toUint8List();
      await setAudioPlayer();
      // TODO GENEREATE WAVE VIEW
      // generateWaveform(audioFile!);
      setProgress(false);
      notifyListeners();
    } catch (e) {
      setProgress(false);
      if (e.runtimeType != FileSelectionCanceledError) {
        DubLog().e('error in pick wav $e');
      }
    }
  }

  Future saveJson() async {
    if (bookModel == null) {
      return;
    }
    ScaffoldMessenger.of(scaffoldKey.currentState!.context).removeCurrentSnackBar();
    try {
      setProgress(true);
      SentencesItem? title = expandedSentences.firstOrNullWhere((element) => element.isTitle);

      if (title != null) {
        bookModel?.title.isTitle = title.isTitle;
        bookModel?.title.isLast = title.isLast;
        bookModel?.title.seqNo = title.seqNo;
        bookModel?.title.text = title.text;
        bookModel?.title.words = title.words;
        bookModel?.title.audio = title.audio;
      }

      String jsonString = jsonEncode(bookModel!.toJson());
      List<int> encodedJsonList = utf8.encode(jsonString);
      var output = Uint8List.fromList(encodedJsonList);
      var now = DateTime.now().toLocal();
      String date = '(${now.year}-${now.month}-${now.day}-${now.hour}:${now.minute})';

      String fileName = '${originfile!.fileName}${date}';
      String dir = await FileSaver.instance.saveFile('$fileName', output, 'json', mimeType: MimeType.JSON);
      setProgress(false);
      ScaffoldMessenger.of(scaffoldKey.currentState!.context).showSnackBar(SnackBar(content: Text('저장되었습니다\n$dir')));
      DubLog().d('convert finished!');
    } catch (e, stack) {
      setProgress(false);
      DubLog().e('error save json $e : $stack');
    }
  }

  Future<List<double>> generateWaveform({required double start, required double end}) async {
    try {
      File file = File('audio.wav');
      file.writeAsBytesSync(audioFile!);
      final response = await FFmpegService.getAudioFilePeakLevel(
        AudioFilePeakLevelInput(audioFile: file, waveformType: WaveformType.accurate),
        start,
        end,
      );
      if (response != null) {
        DubLog().d('generateWaveform : ${response.length}');
        waveForms = response;
        return waveForms!;
      }
      return [];
    } catch (e) {
      DubLog().e('error in get WAVE FORM $e');
      return [];
    }
  }

  Future reset() async {
    stopMainPlayer();
    String json = originfile.toString();
    bookModel = ChapterModel.fromJson(jsonDecode(json));

    selectedPageSeqNo = 1;
    selectedSentenceSeqNo = 1;
    latestPlayTime = DateTime.now();
    historys = [];
    tabType = 'PAGE';
    DubLog().d('Json file reset!');
    notifyListeners();
  }

  Future delayWhilePlayerStart(AudioPlayer player, Duration startPoint) async {
    await player.positionStream.every((pos) {
      if (pos > startPoint) {
        return false;
      } else {
        return true;
      }
    }).timeout(const Duration(seconds: 10), onTimeout: () {
      return true;
    }).onError((error, stackTrace) {
      return true;
    });
  }

  Future pauseAnimationAndPlayer() async {
    latestPlayTime = DateTime.now();
    await player.pause();
    await Future.delayed(Duration(milliseconds: 50));
    setCurrentPos(Duration.zero);
  }

  Future stopMainPlayer({bool? withResetSentence = true}) async {
    await player.pause();
    await Future.delayed(Duration(milliseconds: 50));
    setCurrentPos(Duration.zero);
    if (withResetSentence ?? true) {
      selectedSentenceSeqNo = -1;
      notifyListeners();
    }
  }

  Future<String?> showTextChangeDialog(String initValue) async {
    pauseAnimationAndPlayer();
    return showDialog(
        context: state.context,
        builder: (context) {
          String text = initValue;
          return AlertDialog(
            title: Text('Change word'),
            content: TextField(
              autofocus: true,
              onChanged: (value) {
                text = value;
              },
              onSubmitted: (value) {
                Navigator.pop(context, text);
              },
              controller: TextEditingController(text: text),
              decoration: InputDecoration(hintText: "$initValue"),
            ),
            actions: <Widget>[
              ButtonFlat(
                child: Text(
                  'change',
                  style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                ),
                onPressed: () {
                  Navigator.pop(context, text);
                },
              ),
            ],
          );
        });
  }

  Future<double?> showNumberChangeDialog(double initValue) async {
    pauseAnimationAndPlayer();
    return showDialog(
        context: state.context,
        builder: (context) {
          double number = initValue;
          return AlertDialog(
            title: Text('Change number'),
            content: TextField(
              autofocus: true,
              keyboardType: TextInputType.numberWithOptions(),
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp('[0-9.,]')),
              ],
              onSubmitted: (value) {
                Navigator.pop(context, number);
              },
              onChanged: (value) {
                Map<String, dynamic> numberMap = {};
                numberMap['number'] = value;
                number = asDouble(numberMap, 'number', defaultValue: initValue);
              },
              controller: TextEditingController(text: '${initValue}'),
              decoration: InputDecoration(hintText: "$initValue"),
            ),
            actions: <Widget>[
              ButtonFlat(
                child: Text(
                  'change',
                  style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                ),
                onPressed: () {
                  Navigator.pop(context, number);
                },
              ),
            ],
          );
        });
  }

  void splitByWord({required SentencesItem sentence, required WordsItem word}) {
    pauseAnimationAndPlayer();
    int sentenceSeqNo = sentence.seqNo;
    var splittedWords = sentence.words.splitWhen((p0, p1) => p0 == word).toList();
    if (splittedWords.length <= 1) {
      return;
    }

    List<WordsItem> firstWordList = splittedWords[0];
    List<WordsItem> secondWordList = splittedWords[1];

    String origin = '${sentence.words.map((e) => e.text).join(' ')}';
    String after = '${firstWordList.map((e) => e.text).join(' ')}';

    double currentStartPoint = firstWordList.elementAtOrNull(firstWordList.firstIndex)?.audio.start ?? 0;
    double currentEndPoint = firstWordList.elementAtOrNull(firstWordList.lastIndex)?.audio.end ?? 0;

    SentencesItem currentSentence = SentencesItem(
        audio: Audio(
          start: currentStartPoint,
          end: currentEndPoint,
          duration: max(0, currentEndPoint - currentStartPoint),
        ),
        isLast: true,
        seqNo: sentenceSeqNo,
        text: after,
        words: firstWordList);

    double addedStartPoint = secondWordList.elementAtOrNull(secondWordList.firstIndex)?.audio.start ?? 0;
    double addedEndPoint = secondWordList.elementAtOrNull(secondWordList.lastIndex)?.audio.end ?? 0;

    SentencesItem addedSentence = SentencesItem(
        audio: Audio(
          start: addedStartPoint,
          end: addedEndPoint,
          duration: max(0, addedEndPoint - addedStartPoint),
        ),
        isLast: false,
        seqNo: (sentenceSeqNo + 1),
        text: secondWordList.map((e) => e.text).join(' '),
        words: secondWordList);

    List<ParagraphsItem> paragraphs = [];

    for (ParagraphsItem paragraph in bookModel?.paragraphs ?? []) {
      List<SentencesItem> sentences = [];
      for (SentencesItem sentence in paragraph.sentences) {
        if (sentence.seqNo == sentenceSeqNo) {
          sentences.add(currentSentence);
          if (sentence == paragraph.sentences.last) {
            sentences.add(addedSentence..isLast = true);
          }
        } else if (sentence.seqNo == sentenceSeqNo + 1) {
          if (sentence != paragraph.sentences.first) {
            sentences.add(addedSentence);
          }
          sentences.add(sentence..seqNo = sentence.seqNo + 1);
        } else if (sentence.seqNo > (sentenceSeqNo + 1)) {
          sentence.seqNo = sentence.seqNo + 1;
          sentences.add(sentence);
        } else {
          sentences.add(sentence);
        }
      }
      paragraphs.add(paragraph..sentences = sentences);
    }

    bookModel = ChapterModel(
        title: bookModel!.title, seqNo: bookModel!.seqNo, audioURL: bookModel!.audioURL, paragraphs: paragraphs);

    changeSplit(option: 'split', before: origin, after: after);
  }

  void mergeByWord(
      {required SentencesItem sentence, required SentencesItem previousSentence, required WordsItem word}) {
    pauseAnimationAndPlayer();
    int sentenceSeqNo = previousSentence.seqNo;

    List<WordsItem> mergedWords = [...previousSentence.words, ...sentence.words];

    String origin = '${sentence.words.map((e) => e.text).join(' ')}';
    String after = '${mergedWords.map((e) => e.text).join(' ')}';

    double currentStartPoint = mergedWords.elementAtOrNull(mergedWords.firstIndex)?.audio.start ?? 0;
    double currentEndPoint = mergedWords.elementAtOrNull(mergedWords.lastIndex)?.audio.end ?? 0;

    SentencesItem currentSentence = SentencesItem(
        audio: Audio(
          start: currentStartPoint,
          end: currentEndPoint,
          duration: max(0, currentEndPoint - currentStartPoint),
        ),
        isLast: true,
        seqNo: sentenceSeqNo,
        text: mergedWords.map((e) => e.text).join(' '),
        words: mergedWords);

    List<ParagraphsItem> paragraphs = [];

    for (ParagraphsItem paragraph in bookModel?.paragraphs ?? []) {
      List<SentencesItem> sentences = [];
      for (SentencesItem sentence in paragraph.sentences) {
        if (sentence.seqNo == sentenceSeqNo + 1) {
        } else if (sentence.seqNo == sentenceSeqNo) {
          sentences.add(currentSentence);
        } else if (sentence.seqNo > (sentenceSeqNo + 1)) {
          sentence.seqNo = sentence.seqNo - 1;
          sentences.add(sentence);
        } else {
          sentences.add(sentence);
        }
      }
      paragraphs.add(paragraph..sentences = sentences);
    }

    bookModel = ChapterModel(
        title: bookModel!.title, seqNo: bookModel!.seqNo, audioURL: bookModel!.audioURL, paragraphs: paragraphs);

    changeMerge(option: 'merge', before: origin, after: after);
  }

  void setStartPointInSentence({required SentencesItem sentence, required double applyValue}) {
    pauseAnimationAndPlayer();
    double before = sentence.audio.start;
    double after = applyValue;

    double diff = after - before;

    sentence.audio.start += diff;
    sentence.audio.end += diff;

    changeNumber(option: 'setStartPoint(sentence)', before: before, after: after);
  }

  void setEndPointInSentence({required SentencesItem sentence, required double applyValue}) {
    pauseAnimationAndPlayer();
    double before = sentence.audio.end;
    double after = applyValue;

    double diff = after - before;

    sentence.audio.end += diff;
    sentence.audio.duration += diff;

    changeNumber(option: 'setEndPoint(sentence)', before: before, after: after);
  }

  void setDurationPointInSentence({required SentencesItem sentence, required double applyValue}) {
    pauseAnimationAndPlayer();
    double before = sentence.audio.duration;
    double after = applyValue;

    double diff = after - before;

    sentence.audio.duration += diff;
    sentence.audio.end += diff;

    changeNumber(option: 'setDuration(sentence)', before: before, after: after);
  }

  void setStartPointInWord({required WordsItem word, required double applyValue}) {
    pauseAnimationAndPlayer();
    double before = word.audio.start;
    double after = applyValue;

    double diff = after - before;

    word.audio.start += diff;
    word.audio.end += diff;

    changeNumber(option: 'setStartPoint(word)', before: before, after: after);
  }

  void setEndPointInWord({required WordsItem word, required double applyValue}) {
    pauseAnimationAndPlayer();

    double before = word.audio.end;
    double after = applyValue;

    double diff = after - before;

    word.audio.end += diff;
    word.audio.duration += diff;

    changeNumber(option: 'setEndPoint(word)', before: before, after: after);
  }

  void setDurationPointInWord({required WordsItem word, required double applyValue}) {
    pauseAnimationAndPlayer();
    double before = word.audio.duration;
    double after = applyValue;

    double diff = after - before;

    word.audio.duration += diff;
    word.audio.end += diff;

    changeNumber(option: 'setDuration(word)', before: before, after: after);
  }

  void setTextInWord({required SentencesItem sentence, required WordsItem word, required String applyValue}) {
    pauseAnimationAndPlayer();
    String before = word.text;
    String after = applyValue;

    word.text = applyValue;
    sentence.text = sentence.words.map((e) => e.text).join(' ');

    changeText(option: 'setTextInWord(word)', before: before, after: after);
  }

  void changeBoolean({String option = '', required bool before, required bool after}) {
    historys.add(HistoryModel(option: option, before: before, after: after));
    notifyListeners();
  }

  void changeText({String option = '', required String before, required String after}) {
    historys.add(HistoryModel(option: option, before: before, after: after));
    notifyListeners();
  }

  void changeNumber({String option = '', required double before, required double after}) {
    historys.add(HistoryModel(option: option, before: before, after: after));
    notifyListeners();
  }

  void changeSplit({String option = '', required String before, required String after}) {
    historys.add(HistoryModel(option: option, before: before, after: after));
    notifyListeners();
  }

  void changeMerge({String option = '', required String before, required String after}) {
    historys.add(HistoryModel(option: option, before: before, after: after));
    notifyListeners();
  }
}

class BufferAudioSource extends StreamAudioSource {
  final Uint8List _buffer;

  BufferAudioSource(this._buffer) : super(tag: "audio");

  @override
  Future<StreamAudioResponse> request([int? start, int? end]) {
    start = start ?? 0;
    end = end ?? _buffer.length;

    return Future.value(
      StreamAudioResponse(
        sourceLength: _buffer.length,
        contentLength: end - start,
        offset: start,
        contentType: 'audio/wav',
        stream: Stream.value(List<int>.from(_buffer.skip(start).take(end - start))),
      ),
    );
  }
}
